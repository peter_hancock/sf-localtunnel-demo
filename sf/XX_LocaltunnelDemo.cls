public with sharing class XX_LocaltunnelDemo {

    public String Result {
    	get {
    		String url = ApexPages.CurrentPage().getParameters().get('url');
    		HttpRequest httpRequest = new HttpRequest();
            httpRequest.setMethod('GET');
            httpRequest.setEndpoint(url);
            return new Http().send(httpRequest).getBody();
    	}
    }
}