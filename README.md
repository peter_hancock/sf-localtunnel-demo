Salesforce and localtunnel demo
=======================

Exposing localhost (or anything inside a VPN) to Salesforce with localtunnel

!!! WARNING !!!
The localtunnel proxy is not secured (obviously) - do not expose sensitive unauthenticate endpoints


Demo Setup
----------

```bash
npm install
```

Serving a static file from localhost

```bash
npm start
```

Note the port that this server is listening on and select an ID for your domain (e.g. phancock).  ID can only contain
alphanumeric characters.

```bash
./localtunnel <ID> <PORT>
```
Make a note of the localtunnel URL (something like https://<ID>.localtunnel.me)


Salesforce setup:
-------

Install the assets from  sf/ into the SF Org

Add the localtunnel URL to Setup > Security Controls > Remote Site Settings

Browse https://<SALESFORCE>/apex/XX_LocaltunnelDemo?url=https://<ID>.localtunnel.me
